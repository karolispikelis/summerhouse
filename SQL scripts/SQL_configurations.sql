CREATE TABLE CONFIGURATIONS
(
	ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        Name                                VARCHAR(100),
        Value                               VARCHAR(100),
	OPT_LOCK_VERSION                    INTEGER,
	PRIMARY KEY (ID)
);

INSERT INTO CONFIGURATIONS (Name,Value)
VALUES ('MaxMembersCount','100'); 

INSERT INTO CONFIGURATIONS (Name,Value)
VALUES ('YearlyToll','10'); 

INSERT INTO CONFIGURATIONS (Name,Value)
VALUES ('ReservationGroupCount','4'); 

INSERT INTO CONFIGURATIONS (Name,Value)
VALUES ('ReservationsStartDate','2016-01-11'); 

INSERT INTO CONFIGURATIONS (Name,Value)
VALUES ('ReservationsEndDate','2016-12-31'); 
