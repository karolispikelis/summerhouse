package Controllers;

import DbEntities.Reservations;
import Controllers.util.JsfUtil;
import Controllers.util.PaginationHelper;
import Beans.ReservationsFacade;
import DbEntities.AdditionalServices;
import DbEntities.Members;
import DbEntities.Payments;
import DbEntities.SummerHouses;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

@Named("reservationsController")
@SessionScoped
public class ReservationsController implements Serializable {

    private Reservations current;
    private DataModel items = null;
    @EJB
    private Beans.ReservationsFacade ejbFacade;
    @EJB
    private ConfigurationsController configurationsController;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public ReservationsController() {
    }

    public Reservations getSelected() {
        if (current == null) {
            current = new Reservations();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ReservationsFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "/homePage/home";
    }

    public String prepareView() {
        current = (Reservations) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/reservations/View";
    }

    public String prepareCreate() {
        current = new Reservations();

        selectedItemIndex = -1;
        return "/reservations/Create";
    }

    public String create() {
        try {
            if (configurationsController.isReservationEnabled(current.getMembersId())) {
                getFacade().create(current);
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("ReservationsCreated"));
                recreateModel();
                return prepareCreate();
            } else {
                JsfUtil.addErrorMessage("User cannot create reservation yet. Please wait for your priority start time");
                return null;
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Reservations) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/reservations/Edit";
    }

    public String update() {
        try {
            if (configurationsController.isReservationEnabled(current.getMembersId())) {
                getFacade().edit(current);
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("ReservationsUpdated"));
                return "/reservations/View";
            } else {
                JsfUtil.addErrorMessage("User cannot create reservation yet. Please wait for your priority start time");
                return null;
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Reservations) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "/homePage/home";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "/reservations/View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "/homePage/home";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("ReservationsDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "/homePage/home";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "/homePage/home";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Reservations getReservations(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Reservations.class)
    public static class ReservationsControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ReservationsController controller = (ReservationsController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "reservationsController");
            return controller.getReservations(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Reservations) {
                Reservations o = (Reservations) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Reservations.class.getName());
            }
        }
    }

    public void customCreate(SummerHouses summerHouse, Date fromDate, Date toDate) {
        try {
            if (current.getMembersId().getMembersDate() == null) {
                JsfUtil.addErrorMessage("Nesumokėtas metinis nario mokestis!");
            } else if ((current.getMembersId().getMembersDate().after(Calendar.getInstance().getTime())) && (current.getMembersId().getType() != 2)) {
                if (toDate.after(fromDate)) {
                    if (checkIfAvailable(summerHouse, fromDate, toDate)) {
                        getFacade().create(current);
                        JsfUtil.addSuccessMessage("Rezervacija sukurta.");
                    } else {
                        JsfUtil.addErrorMessage("Vasarnamis pasirinktu laiku užimtas.");
                        return;
                    }
                } else {
                    JsfUtil.addErrorMessage("Neteisingai įvestos rezervacijos datos. Minimalus rezervacijos laikas 1 savaitė.");
                }
            } else {
                JsfUtil.addErrorMessage("Nesumokėtas metinis nario mokestis!");
            }
            current = new Reservations();
            selectedItemIndex = -1;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
        }
    }

    public boolean checkIfAvailable(SummerHouses summerHouse, Date fromDate, Date toDate) {
        if ((fromDate.before(summerHouse.getAvailableFrom())) || (toDate.after(summerHouse.getAvailableTo()))) {
            return false;
        }
        ArrayList<Reservations> reservations = (ArrayList<Reservations>) ejbFacade.findAll();
        if (reservations.isEmpty()) {
            return true;
        }
        for (Reservations res : reservations) {
            if (res.getSummerHousesId().getId() == summerHouse.getId()) {
                if ((res.getReservationFromDate().equals(fromDate)) || (res.getReservationToDate().equals(toDate))) {
                    return false;
                }
                if ((res.getReservationFromDate().before(toDate)) && (res.getReservationToDate().after(toDate))) {
                    return false;
                }
                if ((res.getReservationToDate().after(fromDate)) && (res.getReservationFromDate().before(fromDate))) {
                    return false;
                }
                if ((res.getReservationFromDate().after(fromDate)) && (res.getReservationToDate().before(toDate))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * *
     * Should be changed to return a nice list of available reservation dates.
     * Will leave it at this state for now as I just use it for debugging.
     *
     * @param summerHouse
     * @return
     */
    public boolean[] listAvailableDates(SummerHouses summerHouse) {
        ArrayList<Reservations> reservations = (ArrayList<Reservations>) ejbFacade.findAll();
        ArrayList<String> availableDates = new ArrayList<String>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        int weeksCount = 1 + (int) ((summerHouse.getAvailableTo().getTime() - summerHouse.getAvailableFrom().getTime()) / (1000 * 60 * 60 * 24 * 7));
        boolean[] availability = new boolean[weeksCount];

        for (int j = 0; j < availability.length; j++) {
            availability[j] = true;
        }

        if (reservations.isEmpty()) {
            return availability;
        }
        for (Reservations res : reservations) {
            if (res.getSummerHousesId().getId() == summerHouse.getId()) {
                int start = countWeeks(summerHouse.getAvailableFrom(), res.getReservationFromDate());
                int end = countWeeks(summerHouse.getAvailableFrom(), res.getReservationToDate());
                System.out.println(start + " " + end);
                for (int i = start; i <= end; i++) {
                    availability[i] = false;
                }
            }
        }
        for (boolean b : availability) {
            System.out.println(b);
        }
        return availability;
    }

    public int countWeeks(Date fromDate, Date toDate) {
        return 1 + (int) ((toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24 * 7));
    }

    private Members selectedMember;

    public Members getSelectedMember() {
        return selectedMember;
    }

    public void setSelectedMember(Members selectedMember) {
        this.selectedMember = selectedMember;
    }

    public ArrayList<Reservations> listReservations(Members member) {
        ArrayList<Reservations> reservations = (ArrayList<Reservations>) ejbFacade.findAll();
        ArrayList<Reservations> result = new ArrayList<Reservations>();
        if (member == null) {
            return reservations;
        }
        for (Reservations res : reservations) {
            if (res.getMembersId().getId() == member.getId()) {
                result.add(res);
            }
        }
        return result;
    }

    public void cancelReservation(Reservations reservation) {
        try {
            getFacade().remove(reservation);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("ReservationsDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
        }
        current = null;
    }

    @Inject
    PaymentsController pcontroller;

    public boolean isPaid(Reservations res) {
        ArrayList<Payments> payments = pcontroller.findAllPayments();
        for (Payments p : payments) {
            if (p.getReservationId().getId() == res.getId()) {
                return true;
            }
        }
        return false;
    }

    @Inject
    MembersController mcontroller;

    public ArrayList<Members> filterMembersByDate(Date fromDate, Date toDate) {
        ArrayList<Reservations> reservations = (ArrayList<Reservations>) ejbFacade.findAll();
        ArrayList<Members> members = mcontroller.filterMembers();
        if ((fromDate == null) || (toDate == null)) {
            return members;
        }
        System.out.println(fromDate.toString());
        System.out.println(toDate.toString());

        ArrayList<Members> result = new ArrayList<Members>();
        for (Reservations res : reservations) {
            if ((res.getReservationFromDate().equals(fromDate)) || (res.getReservationToDate().equals(toDate))) {
                System.out.println("Aloha");
                result.add(res.getMembersId());
            } else if ((res.getReservationFromDate().before(toDate)) && (res.getReservationToDate().after(toDate))) {
                result.add(res.getMembersId());
            } else if ((res.getReservationToDate().after(fromDate)) && (res.getReservationFromDate().before(fromDate))) {
                result.add(res.getMembersId());
            } else if ((res.getReservationFromDate().after(fromDate)) && (res.getReservationToDate().before(toDate))) {
                result.add(res.getMembersId());
            }
        }
        return result;
    }

}
