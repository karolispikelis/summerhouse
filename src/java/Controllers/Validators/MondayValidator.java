/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers.Validators;

import java.util.Calendar;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Robertas
 */
@FacesValidator("mondayValidator")
public class MondayValidator implements Validator{

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime((Date)value);
        if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            FacesMessage msg = new FacesMessage("Pradžios data turi būti Pirmadienis!", "Pradžios data turi būti Pirmadienis!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
    
            throw new ValidatorException(msg);
        }
    }
    
}
