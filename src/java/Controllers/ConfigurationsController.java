package Controllers;

import DbEntities.Configurations;
import Controllers.util.JsfUtil;
import Controllers.util.PaginationHelper;
import Beans.ConfigurationsFacade;
import DbEntities.Members;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("configurationsController")
@SessionScoped
public class ConfigurationsController implements Serializable {

    private Configurations current;
    private DataModel items = null;
    @EJB
    private Beans.ConfigurationsFacade ejbFacade;
    @EJB
    private Beans.AdministrationsService administrationsService;
    @EJB
    private Beans.MembersFacade membersFacade;
    private int selectedItemIndex;

    public ConfigurationsController() {
    }

    public Configurations getSelected() {
        if (current == null) {
            current = new Configurations();
            selectedItemIndex = -1;
        }
        return current;
    }

    public void ResetPriorities() {
         try {
            administrationsService.ResetPriorities();
            JsfUtil.addSuccessMessage("Prioritetai buvo sėkmingai perkrauti");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Perkraunant prioritetus įvyko klaida");
        }
    }
    
    public String manageSummerHouses() {
        return "/summerHouses/List";
    }
    
    public String assignPointsForUsers() {
        return "/members/ListAdmin";
    }
    
    public String confirmPayments() {
        return "/payments/ListAdmin";
    }
    
    public void enableReservations() {
         try {
            administrationsService.enableReservations();
            JsfUtil.addSuccessMessage("Reservations enabled");
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
        }
    }
    
    public Boolean isReservationEnabled(Members member) {
         try {
            return administrationsService.isReservationEnabled(member);
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
        }
         return false;
    }
    
     public Boolean isReservationEnabled(int memberId) {
         try {
            return administrationsService.isReservationEnabled(membersFacade.find(memberId));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
        }
         return false;
    }
    
    private ConfigurationsFacade getFacade() {
        return ejbFacade;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareEdit() {
        current = (Configurations) getItems().getRowData();
        selectedItemIndex = getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ConfigurationsUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = new ListDataModel(ejbFacade.findAll());
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Configurations getConfigurations(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Configurations.class)
    public static class ConfigurationsControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ConfigurationsController controller = (ConfigurationsController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "configurationsController");
            return controller.getConfigurations(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Configurations) {
                Configurations o = (Configurations) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Configurations.class.getName());
            }
        }

    }
    
    public int getYearlyToll() {
        return ejbFacade.YearlyToll();
    }
    
    public int getMaxMembers() {
        return ejbFacade.MaxMembersCount();
    }

}
