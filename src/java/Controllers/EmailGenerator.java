/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Controllers.util.JsfUtil;
import DbEntities.Members;
import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.Properties;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Robertas
 */
@Named("emailGenerator")
@SessionScoped
public class EmailGenerator implements Serializable {
    
    public EmailGenerator() { 
    }
    
    //0 - invitation, 1 - confirm my registration    
    public void sendEmail(String to, int emailType) {
        final String username = "best.summer.houses@gmail.com";
        final String password = "kebabaipopitaka";
        
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });
        
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("best.summer.houses@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(setInvitationSubject());
            message.setText(setInvitationText());
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Inject
    MembersController mcontroller;
    
    public void sendRequestEmail(Members candidate, String text, String memberEmail) {
        if (!mcontroller.checkByEmail(memberEmail)) {
            JsfUtil.addErrorMessage("Toks narys neegzistuoja.");
            return;
        }
        
        final String username = "best.summer.houses@gmail.com";
        final String password = "kebabaipopitaka";
        
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });
        
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("best.summer.houses@gmail.com"));
            
            message.setSubject(candidate.getName() + " " + candidate.getSurname() + " narystės patvirtinimo prašymas.");
            message.setText("Prašau patvirtinti mano nario statusą!\n\nPrisistatymas:\n" + text + "\n\n" + "Kandidatas: " + candidate.getName() + " " + candidate.getSurname());
            
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(memberEmail));

            Transport.send(message);
            
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
    
    private String setInvitationSubject() {
        return "Kviečiame prisijungti prie \"Labanoro draugai\" grupės!";
    }
    
    private String setInvitationText() {
        return "Sveiki!\n\nKviečiame jus prisijungti prie grupės \"Labanoro draugai\"! \n\nJoje galėsite rezervuoti vasarnamius atostogoms, blah blah.\n\nhttp://localhost:8080/SummerHouse\n\nLabanoro draugai komanda!";
    }
    
}

