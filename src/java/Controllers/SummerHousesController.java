package Controllers;

import DbEntities.SummerHouses;
import Controllers.util.JsfUtil;
import Controllers.util.PaginationHelper;
import Beans.SummerHousesFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

@Named("summerHousesController")
@SessionScoped
public class SummerHousesController implements Serializable {

    private SummerHouses current;
    private DataModel items = null;
    @EJB
    private Beans.SummerHousesFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
        
    public SummerHousesController() {
    }

    public SummerHouses getSelected() {
        if (current == null) {
            current = new SummerHouses();
            selectedItemIndex = -1;
        }
        return current;
    }

    private SummerHousesFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                        
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (SummerHouses) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new SummerHouses();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("SummerHousesCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (SummerHouses) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("SummerHousesUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (SummerHouses) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("SummerHousesDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public SummerHouses getSummerHouses(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = SummerHouses.class)
    public static class SummerHousesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            SummerHousesController controller = (SummerHousesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "summerHousesController");
            return controller.getSummerHouses(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof SummerHouses) {
                SummerHouses o = (SummerHouses) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + SummerHouses.class.getName());
            }
        }

    }
    
    /***
     * Returns an array of SummerHouses that contains a specified string in their names.
     * If name is an empty string, returns all items.
     */
    public ArrayList<SummerHouses> filterByName(String name) {
        ArrayList<SummerHouses> houses = (ArrayList<SummerHouses>)ejbFacade.findAll();
        ArrayList<SummerHouses> result = new ArrayList<SummerHouses>();
        for (int i = 0; i < houses.size(); i++) {            
            if (houses.get(i).getName().contains(name)) {
                result.add(houses.get(i));
            }
        }
        return result;
    }
    
    /***
     * Returns an array of SummerHouses with a specified amount of maximum guests.
     * If maxGuests is 0, returns all items.
     */
    public ArrayList<SummerHouses> filterByMaxGuests(int maxGuests) {
        ArrayList<SummerHouses> houses = (ArrayList<SummerHouses>)ejbFacade.findAll();
        ArrayList<SummerHouses> result = new ArrayList<SummerHouses>();
        if (maxGuests == 0) {
            return houses;
        }
        for (int i = 0; i < houses.size(); i++) {
            if (houses.get(i).getMaxGuests() == maxGuests) {
                result.add(houses.get(i));
            }
        }
        return result;
    }
    
    /***
     * Returns an array of SummerHouses with a specified price.
     * If price is 0, returns all items.
     */
    public ArrayList<SummerHouses> filterByPrice(int price) {
        ArrayList<SummerHouses> houses = (ArrayList<SummerHouses>)ejbFacade.findAll();
        ArrayList<SummerHouses> result = new ArrayList<SummerHouses>();
        if (price == 0) {
            return houses;
        }
        for (int i = 0; i < houses.size(); i++) {
            if (houses.get(i).getPrice() == price) {
                result.add(houses.get(i));
            }
        }
        return result;
    }
    
    /***
     * Injection needed for the following methods
     */
    @Inject
    ReservationsController rcontroller;
    /***
     * Returns an array of SummerHouses that are available an the specified period of time.
     * If fromDate or toDate are empty, returns all items.
     */
    public ArrayList<SummerHouses> filterByDate(Date fromDate, Date toDate) {
        ArrayList<SummerHouses> houses = (ArrayList<SummerHouses>)ejbFacade.findAll();
        ArrayList<SummerHouses> result = new ArrayList<SummerHouses>();
        
        System.out.println((fromDate == null));
        System.out.println((toDate == null));
        System.out.println(houses.size() + " ");
        if ((fromDate == null) || (toDate == null)) {
            return houses;
        }
        for (int i = 0; i < houses.size(); i++) {
            if (rcontroller.checkIfAvailable(houses.get(i), fromDate, toDate)) {
                result.add(houses.get(i));
            }
        }
        return result;
    }
        
    private String htmlText;
    
    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }
    
    private SummerHouses selectedHouse;

    public SummerHouses getSelectedHouse() {
        return selectedHouse;
    }

    public void setSelectedHouse(SummerHouses selectedHouse) {
        this.selectedHouse = selectedHouse;
    }
    
    @Inject
    MembersController mcontroller;
    
    public void toHtml(int id) {
        SummerHouses house = (SummerHouses)ejbFacade.find(id);
        htmlText = "<h1>Rezervacija</h1></br><h2>" + house.getName() + "<br/>" + mcontroller.getCurrentMember().getName() + "</h2>";
        selectedHouse = house;
    }
    
}
