package Controllers;

import DbEntities.Members;
import Controllers.util.JsfUtil;
import Controllers.util.PaginationHelper;
import Beans.MembersFacade;
import DbEntities.Payments;
import DbEntities.Reservations;
import DbEntities.SummerHouses;
import com.google.gson.Gson;
import java.io.IOException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import static org.apache.openjpa.persistence.jest.ObjectFormatter.dateFormat;
import org.primefaces.context.RequestContext;

@Named("membersController")
@SessionScoped
public class MembersController implements Serializable {

    private Integer additionalPoints;
    private Members current;
    private Members selectedMember;
    private DataModel items = null;
    @EJB
    private Beans.MembersFacade ejbFacade;
    @EJB
    private Beans.ReservationsFacade reservationsFacade;
    @EJB
    private Beans.PaymentsFacade paymentsFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public MembersController() {
    }

    public Integer getAdditionalPoints() {
        return additionalPoints;
    }
    
    public void setAdditionalPoints(Integer points) {
        additionalPoints = points;
    }
    
    public Members getSelectedMember() {

        return selectedMember;
    }
    
    public Members getSelected() {
        if (current == null) {
            current = new Members();
            selectedItemIndex = -1;
        }
        return current;
    }

    private MembersFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "/members/List";
    }

    public String prepareListAdmin() {
        recreateModel();
        return "/members/ListAdmin";
    }

    public String prepareView() {
        current = (Members) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/members/View";
    }

    public String prepareCreate() {
        current = new Members();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("MembersCreated"));
            recreateModel();
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Members) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/members/Edit";
    }

    public String prepareEditAdmin(Members member) {
        selectedMember = member;
        return "/members/EditAdmin";
    }

    public String updateAdmin() {
        try {
            Integer currentPoints = 0;
            if(selectedMember.getPoints() != null){
                currentPoints = selectedMember.getPoints();
            }
            currentPoints = currentPoints + additionalPoints;
            if(currentPoints > 99999){
                JsfUtil.addErrorMessage("Taškų skaičius viršija maksimalų taškų skaičių 99999");
            return null;
            }
            selectedMember.setPoints(currentPoints);
            if(Objects.equals(selectedMember.getId(), current.getId())){
                current.setPoints(currentPoints);
            }
            additionalPoints = null;
            getFacade().edit(selectedMember);
            //JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("MembersUpdated"));
            return "/members/ListAdmin";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    public String update() {
        try {
            getFacade().edit(current);
            //JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("MembersUpdated"));
            return "/members/ListAdmin";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String updateProfile() {
        System.out.println("vyksta");
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("MembersUpdated"));
            return "/profile/profile";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroyMember(Members member) {
        try {
            getFacade().remove(member);
            current = null;
            JsfUtil.addSuccessMessage("Sekmingai išsiregistruota");
            return "/members/MemberDestroyed";
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Nepavyko išsiregistruoti");
        }
        return null;
    }
    
    public String destroy() {
        current = (Members) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "/members/List";
    }

    public String destroyAdmin(Members member) {
       try {
           if(member == current){
               current = null;
           }
            getFacade().remove(member);
            recreateModel();
            JsfUtil.addSuccessMessage("Sekmingai išsiregistruota");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Nepavyko išsiregistruoti");
        }
        return "/members/ListAdmin";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "/members/View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "/members/List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("MembersDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "/members/List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "/members/List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Members getMembers(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    public ArrayList<Members> filterCandidates() {
        ArrayList<Members> members = (ArrayList<Members>) ejbFacade.findAll();
        ArrayList<Members> result = new ArrayList<Members>();
        for (int i = 0; i < members.size(); i++) {
            if (members.get(i).getType() == 2) {
                result.add(members.get(i));
            }
        }
        return result;
    }

    public ArrayList<Members> filterMembers() {
        ArrayList<Members> members = (ArrayList<Members>) ejbFacade.findAll();
        ArrayList<Members> result = new ArrayList<Members>();
        for (int i = 0; i < members.size(); i++) {
            if (members.get(i).getType() != 2) {
                result.add(members.get(i));
            }
        }
        return result;
    }

    @FacesConverter(forClass = Members.class)
    public static class MembersControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            MembersController controller = (MembersController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "membersController");
            return controller.getMembers(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Members) {
                Members o = (Members) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Members.class.getName());
            }
        }
    }

    private String foundInDb(String facebookId) {
        String foundInDb = "false";

        ArrayList<Members> members = (ArrayList<Members>) ejbFacade.findAll();

        for (int i = 0; i < members.size(); i++) {
            if (facebookId.equals(members.get(i).getFacebookId())) {
                foundInDb = "true" + members.get(i).getType();
                current = members.get(i);
                break;
            }
        }

        return foundInDb;
    }

    public Members getCurrentMember() {
        return current;
    }

    public void getMember() {
        String found = "false";
        Gson jsonString = new Gson();
        RequestContext reqCtx = RequestContext.getCurrentInstance();
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String param1 = params.get("param1");
        found = foundInDb(param1);
        reqCtx.addCallbackParam("chartData", jsonString.toJson(found));
    }

    public String createCandidate() {
        try {
            current.setPoints(0);
            current.setType(2);
            current.setMembersDate(Calendar.getInstance().getTime());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Resources").getString("MembersCreated"));
            recreateModel();
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Resources").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    private int profileMemberId;

    public void setProfileMemberId(int profileMemberId) {
        System.out.println("setProfileMemberId");
        this.profileMemberId = profileMemberId;
    }

    public int getProfileMemberId() {
        return profileMemberId;
    }

    public String getProfileName() {
        String name = ejbFacade.find(profileMemberId).getName();
        return name;
    }

    public String getProfileSurName() {
        String surname = ejbFacade.find(profileMemberId).getSurname();
        return surname;
    }

    public String getProfileEmail() {
        String email = ejbFacade.find(profileMemberId).getEmail();
        return email;
    }

    public void disconnect() {
        current = null;
    }

    public void getMemberLoginStatus(int i) throws IOException {
//        if (current.getId() == null) {
//                FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/index_1.html");
//            }
        if (i == 1) {
            if (current.getId() == null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/index_1.html");
            }
        } else if (i == 2) {
            if (current.getId() == null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/index_1.html");
            } else if (current.getType() == 2) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/profile/profile.html");
            }
        } else if (i == 3) {
            if (current.getId() == null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/index_1.html");
            } else if (current.getType() == 2) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/profile/profile.html");
            } else if (current.getType() == 1) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/homePage/home.html");
            }
        }
    }

    public void redirectCandidate() throws IOException {

        if (current.getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/index_1.html");
        } else if (current.getType() == 2) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/profile/profile.html");
        }
    }

    public void redirectMember() throws IOException {

        if (current.getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/index_1.html");
        } else if (current.getType() == 1) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SummerHouse/homepage/home.html");
        }
    }
    
    @Inject
    ConfigurationsController config;
    
    public void confirmCandidate(int memberId) {
        if (getCurrentMember().getType() != 2) {
            if (filterMembers().size() < config.getMaxMembers()) {
                Members temp = current;
                current = ejbFacade.find(memberId);
                current.setType(1);
                update();
                current = temp;
            }
            else {
                JsfUtil.addErrorMessage("Grupėje jau yra maksimalus skaičius narių.");
            }
        }
    }

    public void subtractPoints(int fullPrice) {
        current.setPoints(current.getPoints() - fullPrice);
        update();
    }

    public String dontShowIfCandidate() {
        String disp = "display: block";
        if (current.getType() == 2) {
            disp = "display: none";
        }

        return disp;
    }
    
    public String dontShowIfNotAdmin() {
        String disp = "display: none";
        if (current.getType() == 0) {
            disp = "display: block";
        }

        return disp;
    }

    public String dontShowIfMember() {
        String disp = "display: block";
        if (current.getType() != 2) {
            disp = "display: none";
        }

        return disp;
    }

    private int annualPayment;

   
    
    public void payAnnualPayment() {
        Calendar c = Calendar.getInstance();
        String message = new String();
        
        if (current.getMembersDate() == null) {
            current.setMembersDate(Calendar.getInstance().getTime());
            update();
        }
        
        if ((current.getPoints() >= config.getYearlyToll()) && (current.getMembersDate().before(c.getTime()))) {
            c.add(Calendar.YEAR, 1);
            current.setMembersDate(c.getTime());
            subtractPoints(config.getYearlyToll());
            
            JsfUtil.addSuccessMessage("Klubo nario mokestis sumokėtas");
        } else {
            System.out.println("test");
            if (current.getPoints() <= annualPayment) {
                message = "Nepakanka klubo nario taškų \n";
                System.out.println("test1");
            }
            if (current.getMembersDate().after(c.getTime())) {
                message = "Jūsų nario statusas dar nepasibaigė";
                System.out.println("test2");
            }

            JsfUtil.addErrorMessage(message);
        }
    }

    public String unregister() {
        return "/members/DestroyConfirmation";
    }

    public boolean checkByEmail(String email) {
        ArrayList<Members> members = (ArrayList<Members>)ejbFacade.findAll();
        if (members.isEmpty()) {
            return false;
        }
        for (Members memb : members) {
            if (memb.getEmail().equalsIgnoreCase(email)) {
                return true;
            }
        }
        return false;
    }
    
}
