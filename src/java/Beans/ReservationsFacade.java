/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import DbEntities.Reservations;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

/**
 *
 * @author kpikelis
 */
@Stateless
public class ReservationsFacade extends AbstractFacade<Reservations> {

    @PersistenceContext(unitName = "SummerHousePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReservationsFacade() {
        super(Reservations.class);
    }
    
    public List<Reservations> findAllReservations(Date startDate, Date endDate) {
        List<Reservations> allReservations = em.createNamedQuery("Reservations.findReservationsInRange",Reservations.class)
        .setParameter("reservationFromDate", startDate, TemporalType.DATE)  
        .setParameter("reservationToDate", endDate, TemporalType.DATE)  
        .getResultList();
        return allReservations ;  
  }
}
