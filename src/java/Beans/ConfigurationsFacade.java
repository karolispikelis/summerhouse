/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import DbEntities.Configurations;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kpikelis
 */
@Stateless
public class ConfigurationsFacade extends AbstractFacade<Configurations>{

    @PersistenceContext(unitName = "SummerHousePU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ConfigurationsFacade() {
        super(Configurations.class);
    }
    
    public Integer MaxMembersCount(){
        Configurations maxMembersCountConfig =  (Configurations) em.createNamedQuery("Configurations.findByName").setParameter("name", "MaxMembersCount").getResultList().get(0);
        return Integer.parseInt(maxMembersCountConfig.getValue());
    }
    public Integer YearlyToll(){
        Configurations yearlyTollConfig =  (Configurations) em.createNamedQuery("Configurations.findByName").setParameter("name", "YearlyToll").getResultList().get(0);
        return Integer.parseInt(yearlyTollConfig.getValue());
    }
    public Integer ReservationGroupCount(){
         Configurations reservationGroupCountConfig =  (Configurations) em.createNamedQuery("Configurations.findByName").setParameter("name", "ReservationGroupCount").getResultList().get(0);
        return Integer.parseInt(reservationGroupCountConfig.getValue());
    }
    public Date ReservationsStartDate(){
        try {
            Configurations reservationsStartDateConfig =  (Configurations) em.createNamedQuery("Configurations.findByName").setParameter("name", "ReservationsStartDate").getResultList().get(0);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.parse(reservationsStartDateConfig.getValue());
        } catch (ParseException ex) {
            Logger.getLogger(ConfigurationsFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public Date ReservationsEndDate(){
        try {
            Configurations reservationsEndDateConfig =  (Configurations) em.createNamedQuery("Configurations.findByName").setParameter("name", "ReservationsEndDate").getResultList().get(0);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.parse(reservationsEndDateConfig.getValue());
        } catch (ParseException ex) {
            Logger.getLogger(ConfigurationsFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void setReservationsEndDate(Date date){
         try {
            Configurations reservationsEndDateConfig =  (Configurations) em.createNamedQuery("Configurations.findByName").setParameter("name", "ReservationsEndDate").getResultList().get(0);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            reservationsEndDateConfig.setValue(formatter.format(date));
        } catch (Exception ex) {
            Logger.getLogger(ConfigurationsFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setReservationsStartDate(Date date){
        try {
            Configurations reservationsEndDateConfig =  (Configurations) em.createNamedQuery("Configurations.findByName").setParameter("name", "ReservationsStartDate").getResultList().get(0);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            reservationsEndDateConfig.setValue(formatter.format(date));
        } catch (Exception ex) {
            Logger.getLogger(ConfigurationsFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
