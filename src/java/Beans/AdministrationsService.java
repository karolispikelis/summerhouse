/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import DbEntities.Members;
import DbEntities.Reservations;
import DbEntities.SummerHouses;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kpikelis
 */
@Stateless
public class AdministrationsService {
    @PersistenceContext(unitName = "SummerHousePU")
    private EntityManager em;
    
    @Inject
    private ConfigurationsFacade configurationsFacade;
    
    @Inject
    private MembersFacade membersFacade;
    @Inject
    private SummerHousesFacade summerHousesFacade;
    
     @Inject
    private ReservationsFacade reservationsFacade;
    
    public void ResetPriorities(){
        //get reservation group count
        Integer reservationGroupCount = configurationsFacade.ReservationGroupCount();
        
        List<Members> membersList = membersFacade.findAll();
        int membersInGroup = (int) Math.ceil((double)membersList.size() / reservationGroupCount);

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int year = cal.get(Calendar.YEAR);
        Calendar startDateCalendar = Calendar.getInstance();
        Calendar endDateCalendar = Calendar.getInstance();
        
        startDateCalendar.set(year -1 , 1, 1);
        endDateCalendar.set(year -1 , 12, 31);
        
        
        
        
        //get reservations from previous years
        List<Reservations> reservationsList = reservationsFacade.findAllReservations(startDateCalendar.getTime(), endDateCalendar.getTime());
        //key member id, value how many days reserved
        Map<Members, Integer> membersReserved = new HashMap<Members, Integer>();
        for(int i=0; i<reservationsList.size(); i++){
            Reservations reservation = reservationsList.get(i);
            Members member = reservation.getMembersId();
            
            Calendar calFrom = Calendar.getInstance();
            Calendar calTo = Calendar.getInstance();
            calFrom.setTime(reservation.getReservationFromDate());
            calTo.setTime(reservation.getReservationToDate());
            int daysBetween = calTo.get(Calendar.DAY_OF_YEAR) - calFrom.get(Calendar.DAY_OF_YEAR); 
            
            if(membersReserved.get(member)!=null){
                int previousDays = membersReserved.get(member);
                membersReserved.replace(member, previousDays + daysBetween);
            }
            else{
                membersReserved.put(member, daysBetween);
            }
        }
        //append users with no reservations
        for (int i = 0; i < membersList.size(); i++) {
            Members member = membersList.get(i);
            if(membersReserved.get(member)==null){
                 membersReserved.put(member, 0);
            } 
        }
        
        
        //sort to keep more reserved at the bottom
        Map<Members, Integer> sortedmembersReserved = 
            membersReserved.entrySet().stream()
            .sorted(Entry.comparingByValue())
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue,
                              (e1, e2) -> e1, LinkedHashMap::new));

        int priority = 1;
        int membersCounter = 0;
        for (Map.Entry<Members, Integer> entry : sortedmembersReserved.entrySet()) {
            if(membersCounter == membersInGroup){
                priority++;
                membersCounter=0;
            }
            Members member = entry.getKey();
            member.setPriorityGroup(priority);
            membersCounter++;
        }
    }
    
    public void enableReservations(){
        List<SummerHouses> listSummerHouses = summerHousesFacade.findAll();
        Date minDate = listSummerHouses.stream().map(SummerHouses::getAvailableFrom).min(Date::compareTo).get();
        Date maxDate = listSummerHouses.stream().map(SummerHouses::getAvailableTo).max(Date::compareTo).get();
        
        configurationsFacade.setReservationsStartDate(minDate);
        configurationsFacade.setReservationsStartDate(maxDate);
        
        ResetPriorities();
    }
    
    public Boolean isReservationEnabled(Members member){
        int currentPriority = GetCurrentPriority();
        return member.getPriorityGroup() <= currentPriority;
    }
    
    public int GetCurrentPriority(){
        Calendar reservationsStartDateCalendar = Calendar.getInstance();
        Calendar currentDateCalendar = Calendar.getInstance();
        
        reservationsStartDateCalendar.setTime(configurationsFacade.ReservationsStartDate());
        currentDateCalendar.setTime(new Date());
        
        int startWeek = reservationsStartDateCalendar.get(Calendar.WEEK_OF_YEAR);
        int endWeek = currentDateCalendar.get(Calendar.WEEK_OF_YEAR);

        return endWeek - startWeek;
    }
    
    public Boolean CanUserReservateHouse(int memberId){
        return CanUserReservateHouse(membersFacade.find(memberId));
    }
    
    public Boolean CanUserReservateHouse(Members member){
        return  GetCurrentPriority() >= member.getPriorityGroup();
    }
    
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
